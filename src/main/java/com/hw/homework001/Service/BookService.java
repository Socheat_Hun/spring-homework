package com.kshrd.homework001.Service;

import com.kshrd.homework001.Repository.dto.BookDto;

import java.util.List;

public interface BookService  {

    BookDto insert(BookDto books);

    List<BookDto> select();
}
