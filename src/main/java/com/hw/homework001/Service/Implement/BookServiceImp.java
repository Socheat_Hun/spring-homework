package com.kshrd.homework001.Service.Implement;

import com.kshrd.homework001.Repository.BookRepo;
import com.kshrd.homework001.Repository.dto.BookDto;
import com.kshrd.homework001.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private BookRepo bookRepo;

    @Autowired
    public BookServiceImp(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }


    public BookDto insert(BookDto books) {
         boolean isInserted = bookRepo.insert(books);
         if(isInserted)
             return books;
         else
             return null;
    }

    @Override
    public List<BookDto> select() {
        return bookRepo.select();
    }
}
