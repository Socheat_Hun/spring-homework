package com.kshrd.homework001.rest.Restcontroller;

import com.kshrd.homework001.Repository.dto.BookDto;
import com.kshrd.homework001.rest.Reqeust.BookRequestModel;
import com.kshrd.homework001.rest.Response.baseApiResponse;
import com.kshrd.homework001.Service.Implement.BookServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class BookRestController {
    private BookServiceImp bookService;

    @Autowired
    public BookRestController(BookServiceImp bookServiceImp) {
        this.bookService = bookServiceImp;
    }

    @PostMapping("/articles")
    public ResponseEntity<baseApiResponse<BookRequestModel>> insert(
            @RequestBody BookRequestModel books){
//       validate
        baseApiResponse<BookRequestModel> response = new baseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        BookDto bookDto = mapper.map(books, BookDto.class);
        bookDto.setCategory_id(239042784);
        BookDto result = bookService.insert(bookDto);

        BookRequestModel result2 = mapper.map(result, BookRequestModel.class);
        response.setMessage("You have added article successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @GetMapping("/articles")
    public ResponseEntity<baseApiResponse<List<BookRequestModel>>> select(){
        baseApiResponse<List<BookRequestModel>> response =
                new baseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        List<BookDto> bookDtosList = bookService.select();
        List<BookRequestModel> articles = new ArrayList<>();

        for(BookDto bookDto : bookDtosList){
            articles.add(mapper.map(bookDto, BookRequestModel.class));
        }
        response.setMessage("You have found all articles successfully");
        response.setData(articles);
        response.setStatus(HttpStatus.OK);
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

}
