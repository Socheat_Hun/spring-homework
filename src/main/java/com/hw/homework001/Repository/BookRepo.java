package com.kshrd.homework001.Repository;

import com.kshrd.homework001.Repository.dto.BookDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepo {
    @Insert("INSERT INTO tb_books (id,title,author,description,thumbnail) " +
            "VALUES (#{id},#{title},#{author},#{description},#{thumbnail})")
    boolean insert(BookDto books);

    @Select("SELECT * FROM tb_books")
    List<BookDto> select();
}
